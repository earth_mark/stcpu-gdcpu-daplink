/* Flash OS Routines (Automagically Generated)
 * Copyright (c) 2009-2015 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

static const uint32_t STM32F103XX_flash_prog_blob[] = {
    0xE00ABE00, 0x062D780D, 0x24084068, 0xD3000040, 0x1E644058, 0x1C49D1FA, 0x2A001E52, 0x4770D1F2,
    0x49464847, 0x49476041, 0x20006041, 0x49444770, 0x60c82034, 0x47702000, 0x47702000, 0x69084940,
    0x0f80f010, 0x483dd003, 0x483e6048, 0x68c86048, 0x0f01f010, 0x6908d1fb, 0x0004f040, 0x69086108,
    0x0040f040, 0x68c86108, 0x0f01f010, 0x6908d1fb, 0x0004f020, 0x20006108, 0x4a314770, 0xf0116911,
    0xd0030f80, 0x6051492d, 0x6051492e, 0xf01168d1, 0xd1fb0f01, 0xf0416911, 0x61110102, 0x69106150,
    0x0040f040, 0x68d06110, 0x0f01f010, 0x6910d1fb, 0x0002f020, 0x20006110, 0xb4304770, 0x23004c20,
    0xc010f8d4, 0x0f80f01c, 0xf8dfd007, 0xf8c4c070, 0xf8dfc004, 0xf8c4c070, 0xf8d4c004, 0xf01cc00c,
    0xd1fa0f01, 0x0f51ebb3, 0xf8d4d220, 0xf04cc010, 0xf8c40c01, 0xf8b2c010, 0xf8a0c000, 0xf8d4c000,
    0xf01cc00c, 0xd1fa0f01, 0xc000f8b2, 0x45ac8805, 0x6920d006, 0x0001f020, 0xbc306120, 0x47702001,
    0x1c921c80, 0xebb31c5b, 0xd3de0f51, 0xf0206920, 0x61200001, 0x2000bc30, 0x00004770, 0x45670123,
    0x40022000, 0xcdef89ab, 0x00000000
};

// Start address of flash
static const uint32_t flash_start = 0x08000000;
// Size of flash
static const uint32_t flash_size = 0x00080000;

/**
* List of start and size for each size of flash sector - even indexes are start, odd are size
* The size will apply to all sectors between the listed address and the next address
* in the list.
* The last pair in the list will have sectors starting at that address and ending
* at address flash_start + flash_size.
*/
static const uint32_t sectors_info[] = {
    0x08000000, 0x00000800,
};

static const program_target_t flash = {
    0x2000002f, // Init
    0x20000039, // UnInit
    0x2000003d, // EraseChip
    0x2000007b, // EraseSector
    0x200000bb, // ProgramPage

    // BKPT : start of blob + 1
    // RSB  : blob start + header + rw data offset
    // RSP  : stack pointer
    {
        0x20000001,
        0x20000148,
        0x20000800
    },

    0x20000000 + 0x00000A00,  // mem buffer location
    0x20000000,               // location to write prog_blob in target RAM
    sizeof(STM32F103XX_flash_prog_blob),   // prog_blob size
    STM32F103XX_flash_prog_blob,           // address of prog_blob
    0x00000800       // ram_to_flash_bytes_to_be_written
};
